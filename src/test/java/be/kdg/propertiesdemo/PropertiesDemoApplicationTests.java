package be.kdg.propertiesdemo;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PropertiesDemoApplicationTests {
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesDemoApplicationTests.class);

	@Autowired
	private PropertiesConfiguration configuration;

	@Test
	void contextLoads() {
	}
}
