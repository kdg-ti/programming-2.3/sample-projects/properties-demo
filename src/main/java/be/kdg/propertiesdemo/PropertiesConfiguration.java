package be.kdg.propertiesdemo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("custom")
public class PropertiesConfiguration {
    private String property;
    private String propertyNotOverridden;
    private String testProperty;
    private String testPropertyNotAvailableInTestSourceSet;
    private String propertyNotAvailableInTestSourceSet;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getPropertyNotOverridden() {
        return propertyNotOverridden;
    }

    public void setPropertyNotOverridden(String propertyNotOverridden) {
        this.propertyNotOverridden = propertyNotOverridden;
    }

    public String getTestProperty() {
        return testProperty;
    }

    public void setTestProperty(String testProperty) {
        this.testProperty = testProperty;
    }

    public String getPropertyNotAvailableInTestSourceSet() {
        return propertyNotAvailableInTestSourceSet;
    }

    public void setPropertyNotAvailableInTestSourceSet(String propertyNotAvailableInTestSourceSet) {
        this.propertyNotAvailableInTestSourceSet = propertyNotAvailableInTestSourceSet;
    }

    public String getTestPropertyNotAvailableInTestSourceSet() {
        return testPropertyNotAvailableInTestSourceSet;
    }

    public void setTestPropertyNotAvailableInTestSourceSet(String testPropertyNotAvailableInTestSourceSet) {
        this.testPropertyNotAvailableInTestSourceSet = testPropertyNotAvailableInTestSourceSet;
    }
}
