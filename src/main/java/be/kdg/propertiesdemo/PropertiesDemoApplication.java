package be.kdg.propertiesdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PropertiesDemoApplication implements CommandLineRunner {
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesDemoApplication.class);

	private final PropertiesConfiguration configuration;

	@Autowired
	public PropertiesDemoApplication(PropertiesConfiguration configuration) {
		this.configuration = configuration;
	}

	public static void main(String[] args) {
		SpringApplication.run(PropertiesDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		LOGGER.info("The property's value is: '" + configuration.getProperty() + "'");
		LOGGER.info("The not overridden property's value is: '" + configuration.getPropertyNotOverridden() + "'");
		LOGGER.info("The test property's value is: '" + configuration.getTestProperty() + "'");
		LOGGER.info("The value of the property that's not available in the test source set is: '" + configuration.getPropertyNotAvailableInTestSourceSet() + "'");
		LOGGER.info("The value of the test property that's not available in the test source set is: '" + configuration.getTestPropertyNotAvailableInTestSourceSet() + "'");
	}
}
