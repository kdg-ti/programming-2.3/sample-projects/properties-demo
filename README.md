# Spring Boot Profiles and Properties Demo

## Examples

### Running without a profile

Command:
```shell
./gradlew bootRun
```

Expected output:
```
The property's value is: 'defaultValue'
The not overridden property's value is: 'defaultValue'
The test property's value is: 'null'
The value of the property that's not available in the test source set is: 'defaultValue'
The value of the test property that's not available in the test source set is: 'null'
```

### Running with the dev profile

Command:
```shell
spring_profiles_active=dev ./gradlew bootRun
```

Expected output:
```
The property's value is: 'devValue'
The not overridden property's value is: 'defaultValue'
The test property's value is: 'null'
The value of the property that's not available in the test source set is: 'defaultValue'
The value of the test property that's not available in the test source set is: 'null'
```

### Running tests without a profile

Command:
```shell
./gradlew test
```

Expected output:
```
The property's value is: 'valueFromTestSourceSet'
The not overridden property's value is: 'valueFromTestSourceSet'
The test property's value is: 'null'
The value of the property that's not available in the test source set is: 'null'
The value of the test property that's not available in the test source set is: 'null'
```

### Running tests with the test profile

Command:
```shell
spring_profiles_active=test ./gradlew test
```

Expected output:
```
The property's value is: 'testValueFromTestSourceSet'
The not overridden property's value is: 'valueFromTestSourceSet'
The test property's value is: 'testValueFromTestSourceSet'
The value of the property that's not available in the test source set is: 'null'
The value of the test property that's not available in the test source set is: 'null'
```
